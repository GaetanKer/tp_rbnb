-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lamp
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `id_room` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bookings_users_idx` (`id_user`),
  KEY `FK_booking_rooms_idx` (`id_room`),
  CONSTRAINT `FK_booking_rooms` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id`),
  CONSTRAINT `FK_bookings_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (1,'2021-06-04','2021-06-05',1,2),(2,'1998-12-03','1998-12-05',2,2),(3,'2053-05-25','2054-05-25',3,4),(4,'2024-03-24','2024-04-25',1,4),(5,'2021-06-11','2021-06-12',1,2),(6,'2021-07-10','2021-07-11',3,2),(7,'2021-06-25','2021-06-27',2,4);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipments`
--

DROP TABLE IF EXISTS `equipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipments`
--

LOCK TABLES `equipments` WRITE;
/*!40000 ALTER TABLE `equipments` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(50) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `type` int(1) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `sleeping` int(11) NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id`),
  KEY `FK_rooms_users_idx` (`id_user`),
  CONSTRAINT `FK_rooms_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'9 rue de la raie publique, Australie, Sidney',15.64,1,60,3,1,'C\'est une maison vu sur la déchetterie'),(2,'6 rue lamaid, Espagne, Malaga',1500.00,0,5000,12,1,'Ouf'),(3,'3 avenue des anges,France, Perpignan',250.37,2,250,6,3,'Bien placé pour aller aux événements locaux'),(4,'4 rue des boites, Maroc, Rabat',150.00,2,60,4,1,'Belle vue sur la Palmeraie');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_equipments`
--

DROP TABLE IF EXISTS `rooms_equipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms_equipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rooms_id` int(10) unsigned NOT NULL,
  `equipment_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rooms_equipments_rooms_idx` (`rooms_id`),
  KEY `FK_rooms_equipments_equipments_idx` (`equipment_id`),
  CONSTRAINT `FK_rooms_equipments_equipments` FOREIGN KEY (`equipment_id`) REFERENCES `equipments` (`id`),
  CONSTRAINT `FK_rooms_equipments_rooms` FOREIGN KEY (`rooms_id`) REFERENCES `rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_equipments`
--

LOCK TABLES `rooms_equipments` WRITE;
/*!40000 ALTER TABLE `rooms_equipments` DISABLE KEYS */;
/*!40000 ALTER TABLE `rooms_equipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` int(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'toto','efdd6c7c50600fd201838cb047ffa1dc797f026760001b3899f3f586640e54ee8d64deb4d6e2bf42fd096c8ceefd6d9ebb44a2f996979871d0e806f59ffa1974',0),(2,'charles','efdd6c7c50600fd201838cb047ffa1dc797f026760001b3899f3f586640e54ee8d64deb4d6e2bf42fd096c8ceefd6d9ebb44a2f996979871d0e806f59ffa1974',1),(3,'martin','efdd6c7c50600fd201838cb047ffa1dc797f026760001b3899f3f586640e54ee8d64deb4d6e2bf42fd096c8ceefd6d9ebb44a2f996979871d0e806f59ffa1974',0),(4,'aurelien','efdd6c7c50600fd201838cb047ffa1dc797f026760001b3899f3f586640e54ee8d64deb4d6e2bf42fd096c8ceefd6d9ebb44a2f996979871d0e806f59ffa1974',1),(5,'mathend','efdd6c7c50600fd201838cb047ffa1dc797f026760001b3899f3f586640e54ee8d64deb4d6e2bf42fd096c8ceefd6d9ebb44a2f996979871d0e806f59ffa1974',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-06 16:14:14
