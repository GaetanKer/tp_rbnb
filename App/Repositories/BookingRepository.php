<?php

namespace App\Repositories;

use Exception;

use App\Models\Booking;
use App\Models\Room;

class BookingRepository extends Repository
{
    public function getColumns(): array
	{
		return ['date_start', 'date_end', 'id_room', 'id_user' ];
	}
    
	public function getTable(): string
	{
		return 'bookings';
	}

	// cRud - READ - Toutes les voitures en données brutes de la table "bookings"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Booking::class, $query_addons, $addon_data );
	}

	// cRud - READ - Une voiture en données brutes de la table "bookings"
	public function findById( int $id ): ?Booking
	{
		return $this->readById( Booking::class, $id );
	}

    public function findByUserId( int $id_user ): array
    {

        $result = [];

        $q = 'SELECT 
        b.id,
        b.date_start,
        b.date_end,
        b.id_room,
        r.address
        FROM bookings AS b
                JOIN rooms AS r ON b.id_room = r.id
                WHERE b.id_user=:id_user';
;

        
        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id_user' => $id_user ] );
    
                while( $data = $stmt->fetch() ) {
                    // new $class_name() instancie une classe dont le nom est contenu dans $class_name
                    array_push( $result, $data);
                    
                }
		}

		return $result;
    }

    public function findByOwnerId( int $id_user ): array
    {

        $result = [];

        $q = 'SELECT 
        b.id,
        b.date_start,
        b.date_end,
        b.id_room,
        r.address,
        u.user_name
        FROM bookings AS b
                JOIN rooms AS r ON b.id_room = r.id
                JOIN users AS u ON b.id_user = u.id
                WHERE r.id_user=:id_user';
;

        
        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id_user' => $id_user ] );
    
                while( $data = $stmt->fetch() ) {

                    array_push( $result, $data);
                    
                }
		}

		return $result;
    }

}