<?php

namespace App\Repositories;

use Exception;

use App\Models\Room;

class RoomRepository extends Repository
{
	public function getColumns(): array
	{
		return ['address', 'price', 'type', 'size', 'sleeping', 'id_user', 'description' ];
	}

	public function getTable(): string
	{
		return 'rooms';
	}

	// cRud - READ - Toutes les voitures en données brutes de la table "Room"
	public function findAll( array $query_addons = [], array $addon_data = [] ): array
	{
		return $this->readAll( Room::class, $query_addons, $addon_data );
	}

	// cRud - READ - Une voiture en données brutes de la table "room"
	public function findById( int $id ): ?Room
	{
		return $this->readById( Room::class, $id );
	}

    public function findByUserId( int $id_user, array $query_addons = [] ): array
    {

        $model_class = Room::class;

        $result = [];

        $q = 'SELECT * FROM '. $this->getTable() .' WHERE id_user=:id_user';

        $stmt = $this->pdo->prepare( $q );


			if( !$stmt ) {
                throw new Exception( 'Une erreur s\'est produite' );
            }
            else {
                $stmt->execute( [ 'id_user' => $id_user ] );
    
                while( $data = $stmt->fetch() ) {
                    // new $class_name() instancie une classe dont le nom est contenu dans $class_name
                    array_push( $result, new $model_class( $data ) );
                }
		}

		return $result;
    }

	public function findThree( array $query_addons = [], array $addon_data = [] ): array
	{
		{
			$model_class = Room::class;

			$result = [];
	
			$q = 'SELECT * 
			FROM '. $this->getTable() .'
			LIMIT 3';

			if( !empty( $query_addons ) ) {
				$q .= ' ' . implode( ' ', $query_addons );
			}
	
			$stmt = $this->pdo->prepare( $q );
	
			if( !$stmt ) {
				throw new Exception( 'Une erreur s\'est produite' );
			}
			else {
				$stmt->execute( $addon_data );
	
				while( $data = $stmt->fetch() ) {
					// new $class_name() instancie une classe dont le nom est contenu dans $class_name
					array_push( $result, new $model_class( $data ) );
				}
			}
	
			return $result;
		}
	}
	
}