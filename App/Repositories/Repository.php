<?php

namespace App\Repositories;

use Exception;
use PDO;
use PDOException;

use App\Models\Model;
use App\PdoDatabase;



abstract class Repository
{
	protected PDO $pdo;

	abstract public function getColumns(): array;
	abstract public function getTable(): string;
	abstract public function findAll(): array;
	abstract public function findById( int $id ): ?Model;

	public function __construct()
	{
		$this->pdo = PdoDatabase::getPdo();
	}

	/* --- CRUD --- */

	/**
	 * Crud: Create
	 * @param Model $model
	 * @return Model
	 */
	public function create( Model $model ): Model
	{
		// ex: INSERT INTO `cars` (`color`,`title`,`desc`) VALUES (:color,:title,:desc)
		$q = sprintf(
			'INSERT INTO `%s` (%s) VALUES (%s)',
			$this->getTable(),
			$this->getQueryColumnsList(),
			$this->getQueryValuesList()
		);

		$stmt = $this->pdo->prepare( $q );
		$stmt->execute( $this->getQueryBindings( $model ) );

		if( $stmt->errorCode() != PDO::ERR_NONE ) {
			throw new PDOException( $stmt->errorInfo()[2], $stmt->errorInfo()[1] );
		}

		$model->id = $this->pdo->lastInsertId();

		return $model;
	}



	/**
	 * CRUD: Read sur tous les éléments
	 * @return array
	 */
	protected function readAll( string $model_class, array $query_addons = [], array $addon_data = [] ): array
	{
		$result = [];

		$q = 'SELECT * FROM '. $this->getTable();
		if( !empty( $query_addons ) ) {
			$q .= ' ' . implode( ' ', $query_addons );
		}

		$stmt = $this->pdo->prepare( $q );

		if( !$stmt ) {
			throw new Exception( 'Une erreur s\'est produite' );
		}
		else {
			$stmt->execute( $addon_data );

			while( $data = $stmt->fetch() ) {
				// new $class_name() instancie une classe dont le nom est contenu dans $class_name
				array_push( $result, new $model_class( $data ) );
			}
		}

		return $result;
	}

	protected function readById( string $model_class, int $id ): ?Model
	{
		$result = null;

		$q = 'SELECT * FROM '. $this->getTable() .' WHERE id=:id';

		$stmt = $this->pdo->prepare( $q );

		if( $stmt ) {
			$stmt->execute([ 'id' => $id ]);

			$data = $stmt->fetch();

			// $stmt->errorInfo()[1] est null si il n'y pas d'erreur, sinon contient le code de l'erreur
			if( !is_null( $stmt->errorInfo()[1] ) ) {
				throw new Exception( 'Une erreur s\'est produite' );
			}
			// $data est false si il n'y a pas de résultat
			else if( !empty( $data ) ) {
				$result = new $model_class( $data );
			}
		}

		return $result;
	}

	protected function getQueryBindings( Model $model ): array
	{
		$bindings = [];

		foreach( $this->getColumns() as $column ) {
			if( property_exists( $model, $column ) ) {
				// "??": "Null Coalesce operator"
				$bindings[$column] = $model->$column ?? null ; // equiv: isset( $model->$column ) ? $model->column : null
			}
		}

		return $bindings;
	}

	protected function getQueryColumnsList(): string
	{
		return implode( ',', array_map( function( $item ) { return '`'. $item .'`'; }, $this->getColumns() ) );
	}

	protected function getQueryValuesList(): string
	{
		return implode( ',', array_map( function( $item ) { return ':'. $item; }, $this->getColumns() ) );
	}

}