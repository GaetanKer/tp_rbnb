<?php

namespace App\Repositories;

class RepositoryManager
{
	private static ?self $instance = null;

	private UserRepository $userRepository;
	public function getUserRepository(): UserRepository { return $this->userRepository; }

	private RoomRepository $roomRepository;
	public function getRoomRepository(): RoomRepository { return $this->roomRepository; }

	private BookingRepository $bookingRepository;
	public function getBookingRepository(): BookingRepository { return $this->bookingRepository; }

	public static function getRm(): self
	{
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct()
	{
		$this->userRepository = new UserRepository();
		$this->roomRepository = new RoomRepository();
		$this->bookingRepository = new BookingRepository();
	}

	private function __clone() { }
	private function __wakeup() { }
}