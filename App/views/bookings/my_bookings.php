<h1>Mes réservations</h1>

<?php if( empty( $bookings ) ): ?>
	<div>Aucune réservation trouvée</div>
<?php else: ?>
	<ul>
		<?php foreach( $bookings as $booking ): ?>
			<li>
				<h2>
					<a href="chambres/<?php echo $booking['id_room']?>"><?php echo $booking['address']?></a>
				</h2>
				<p> début de réservation : <?php echo $booking['date_start'] ?></p>
				<p> fin de réservation : <?php echo $booking['date_end'] ?></p>
                
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
