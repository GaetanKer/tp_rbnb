<h1>Notre site de réservation</h1>

<h2>nos chambres</h2>

<?php if( empty( $rooms ) ): ?>
	<div>Aucune chambre trouvée :'(</div>
<?php else: ?>
	<ul>
		<?php foreach( $rooms as $room ): ?>
			<li>

				<h3>
					<a href="/chambres/<?php echo $room->id ?>">
						<?php echo $room->address ?>
					</a>
				</h3>
				<p> <?php echo $room->price ?> €</p>
				<p> <?php echo $room->size ?> m²</p>
                
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>

<a href="/chambres"><p class="button">voir plus</p></a>