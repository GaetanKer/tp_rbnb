<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php

use App\Models\User;

echo $html_title ?></title>

    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<header>
		<div>
			<?php if( self::isAuth() ): ?>
				Bonjour, <?php echo self::authUser()->user_name ?> - <a href="/deconnexion">Déconnecter</a>
			<?php else: ?>
                Vous-avez un compte ? - <span class="link"> <a href="/connexion">Connectez-vous</a> </span>, sinon <span class="link"><a href="/inscription">Créez en un</a> </span>
			<?php endif; ?>
		</div>

		<nav>
			<ul>
				<li><a href="/">accueil</a></li>
				<li><a href="/chambres">nos chambres</a></li>

				<?php if( self::isAuth() ): ?>

					<?php if (self::authUser()->role == 0): ?>
					<li><a href="/mes_annonces">mes annonces</a></li>
					<li><a href="/mes_chambres_reservees">mes chambres réservées</a></li>
					<?php endif; ?>

					<?php if (self::authUser()->role == 1): ?>
					<li><a href="/mes_reservations">mes réservations</a></li>
					<?php endif;?>
				
				<?php endif;?>
				
			</ul>
		</nav>
	</header>