<a href="/chambres">&lt; Liste</a>

<h1><?php

echo $room->address ?></h1>
<p>
	<?php echo $room->description ?>
</p>
<p>
	<?php echo $room->price ?> €
</p>
<p>
	<?php echo $room->size ?> m²
</p>
<p>
	<?php if($room->type == 0): ?>
        Logement entier
    <?php endif; ?>
</p>

<p>
	<?php if($room->type == 1): ?>
        Chambre privée
    <?php endif; ?>
</p>

<p>
	<?php if($room->type == 2): ?>
        Chambre partagée
    <?php endif; ?>
</p>

<p>
	Nombre de couchages : <?php echo $room->sleeping ?>
</p>

<?php if( self::isAuth() && self::authUser()->role == 1): ?>

<form method="post" action="">
<div>

<label for="date_start">Début de votre séjour :</label> 
<input type="date" name="date_start" id="date_start" placeholder="début de la réservation"> <br>
<label for="date_end">Fin de votre séjour :</label>
<input type="date" name="date_end" id="date_end" placeholder="fin de la réservation">

</div>
<input type="hidden" name="id_room" value="<?php echo $room->id ?>">
<input type="hidden" name="id_user" value="<?php echo self::authUser()->id?>">

<input type="submit" value="réserver">
</form>

<?php endif;?>