<h1>Ajouter une chambre</h1>

<form method="post" action="">
    <input type="text" name="address" placeholder="adresse de la chambre"><br>
    
    <input type="number" name="price" placeholder="prix"><br>

    <div>
        <label for="entire_room">Logement entier</label>
        <input type="radio" name="type" id="entire_room" value="entire_room">
    </div>

    <div>
        <label for="private_room">Chambre privée</label>
        <input type="radio" name="type" id="private_room" value="private_room">
    </div>

    <div>
        <label for="shared_room">Chambre partagée</label>
        <input type="radio" name="type" id="shared_room" value="shared_room">
    </div>

    <input type="number" name="size" placeholder="surface en m²"><br>
    <input type="number" name="sleeping" placeholder="nombre de couchages"><br>
    <textarea name="description" placeholder="description du logement"
          rows="5" cols="33"></textarea><br>

    <input type="hidden" name="id_user" value="<?php echo self::authUser()->id?>">

    <input type="submit" value="enregistrer">
    
</form>