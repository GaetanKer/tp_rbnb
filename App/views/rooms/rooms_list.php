<h1> Liste de nos chambres</h1>

<?php if( empty( $rooms ) ): ?>
	<div>Aucune chambre trouvée :'(</div>
<?php else: ?>
	<ul>
		<?php foreach( $rooms as $room ): ?>
			<li>
				<h2>
					<a href="/chambres/<?php echo $room->id ?>">
						<?php echo $room->address ?>
					</a>
				</h2>
				<p> <?php echo $room->price ?> €</p>
				<p> <?php echo $room->size ?> m²</p>
                
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>