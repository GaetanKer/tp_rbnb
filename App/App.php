<?php

namespace App;

use MiladRahimi\PhpRouter\Exceptions\InvalidCallableException;
use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
use MiladRahimi\PhpRouter\Router;

use App\Controllers\PageController;
use App\Controllers\RoomController;
use App\Controllers\BookingController;

class App
{
	/**
	 * @var App|null Singleton instance of the application
	 */
	private static ?self $instance = null;

	/**
	 * @var Router Router instance used in the application
	 */
	private Router $router;

	/**
	 * Gets the singleton instance of the application
	 *
	 * @return App Singleton instance
	 */
	public static function getApp(): self
	{
		if( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Starts the application
	 */
	public function start(): void
	{
		session_start();

		$this->initRouter();
	}

	/**
	 * Router initialization
	 */
	private function initRouter(): void
	{
		$this->router = Router::create();
		$this->registerRoutes();

		try {
			$this->router->dispatch();
		}
		catch( RouteNotFoundException $e_404 ) {
			View::render404();
		}
		catch( InvalidCallableException $e_invalid ) {
			View::render500();
		}
	}

	/**
	 * Registers the routes to the router
	 */
	private function registerRoutes(): void
	{
		// Patterns for routes arguments
		$this->router->pattern('id', '\d+');

		// Routes (ici on rajoute nos nouvelles routes, en appelant la fonction de PageController)
		$this->router->get( '/', [ PageController::class, 'index' ] );
		$this->router->get( '/connexion', [ PageController::class, 'login' ] );
		$this->router->post( '/connexion', [ PageController::class, 'processLogin' ]);
		$this->router->get( '/deconnexion', [ PageController::class, 'logout' ]);
		$this->router->get( '/inscription', [ PageController::class, 'register' ] );
		$this->router->post( '/inscription', [ PageController::class, 'add_user' ] );
		
		$this->router->get( '/chambres', [ RoomController::class, 'index' ] );
		$this->router->get( '/mes_annonces', [ RoomController::class, 'my_ads' ] );
		$this->router->get( '/ajout_chambre', [ RoomController::class, 'add_room' ] );
		$this->router->post( '/ajout_chambre', [ RoomController::class, 'register_room' ] );
		$this->router->get( '/chambres/{id}', [ RoomController::class, 'show' ]);
		$this->router->post( '/chambres/{id}', [ BookingController::class, 'add_booking' ]);

		
		$this->router->get( '/mes_reservations', [ BookingController::class, 'my_bookings' ]);
		$this->router->get( '/mes_chambres_reservees', [ BookingController::class, 'my_booking_rooms' ]);

	}

	// Singleton pattern locks
	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}
}