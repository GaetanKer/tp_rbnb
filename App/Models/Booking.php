<?php

namespace App\Models;

use App\Repositories\RepositoryManager;

class Booking extends Model
{
	// Propriétés issues des colonnes de la table "cars"
	public string $date_start;
	public string $date_end;
	public int $id_room;
	public int $id_user;

}