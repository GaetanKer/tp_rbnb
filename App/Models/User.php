<?php

/**
 * Class User
 */

namespace App\Models;

class User extends Model
{
    public const ROLE_OWNER = 0;
    public const ROLE_USER = 1;
    
    public string $user_name;
    public string $password;
    public int $role;

    public function getRoleName(): string
    {
        $role_name = '';

        switch( $this->role ) {
            case self::ROLE_OWNER:
                $role_name = 'Annonceur';
                break;

            case self::ROLE_USER:
                $role_name = 'Utilisateur';
                break;

            default:
                $role_name = 'Pas connecté';
                break;
        }

        return $role_name;
    }

    public static function hashPassword( string $password ): string
	{
		return hash('sha512', HASH_SALT.$password.HASH_PEPPER );
	}

    public static function fromSession(): ?self
	{
    	if( ! self::isAuth() ) {
    		return null;
		}

    	return $_SESSION[ 'USER' ];
	}

	public static function isAuth(): bool
	{
		return isset( $_SESSION[ 'USER' ] );
	}
}