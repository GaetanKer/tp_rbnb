<?php

namespace App\Models;

class Room extends Model
{
	// Propriétés issues des colonnes de la table "cars"
	public string $address;
	public float $price;
	public int $type;
	public int $size;
	public int $sleeping;
	public int $id_user;
    public string $description;

}