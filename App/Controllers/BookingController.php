<?php

namespace App\Controllers;

use App\Models\Booking;
use App\Repositories\RepositoryManager;
use App\View;
use Laminas\Diactoros\ServerRequest;
use PDOException;

class BookingController 
{
    public function my_bookings(): void
	{

		if( View::isAuth() && View::authUser()->role == 1) {

			$bookings = RepositoryManager::getRm()->getBookingRepository()->findByUserId( View::authUser()->id );

			$view = new View( 'bookings\my_bookings' );
			
			$view->render([
				'html_title' => 'ernBnB - mes reservation',
				'bookings' => $bookings
			]);
		}

		else {
			View::render404();
		}

	}

    public function my_booking_rooms(): void
	{
		if( View::isAuth() && View::authUser()->role == 0) {

			$bookings = RepositoryManager::getRm()->getBookingRepository()->findByOwnerId( View::authUser()->id );

			$view = new View( 'bookings\my_booking_rooms' );
			
			$view->render([
				'html_title' => 'ernBnB - mes chambres réservées',
				'bookings' => $bookings
			]);
		}

		else {
			View::render404();
		}
		

		$view->render([
			
		]);
	}

	public function add_booking( ServerRequest $request ): void {
		$post_data = $request->getParsedBody();
		$booking = new Booking();
		$booking->date_start = $post_data['date_start'];
		$booking->date_end = $post_data['date_end'];
		$booking->id_room = $post_data['id_room'];
		$booking->id_user = $post_data['id_user'];

		
		// Utilisation de create
		$created_booking = null;

		try {
			$created_booking = RepositoryManager::getRm()->getBookingRepository()->create( $booking );
		}
		catch( PDOException $e ) {
			var_dump( $e );
			View::render500();
		}

		header( 'Location: /chambres/' . $post_data['id_room'] );
	}
}