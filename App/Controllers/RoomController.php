<?php

namespace App\Controllers;

use App\Models\Room;
use App\Repositories\RepositoryManager;
use App\View;
use Laminas\Diactoros\ServerRequest;
use PDOException;

class RoomController
{
    public function index(): void
	{

		$view = new View( 'rooms\rooms_list' );

		$view_data = [
			'html_title' => 'ernBnB - liste des chambres',
			'rooms' => RepositoryManager::getRm()->getRoomRepository()->findAll()
		];

		$view->render( $view_data );
	}

    public function my_ads( ): void
	{
		if( View::isAuth() && View::authUser()->role == 0) {

			$rooms = RepositoryManager::getRm()->getRoomRepository()->findByUserId( View::authUser()->id );

			$view = new View( 'rooms\my_rooms_list' );
			
			$view->render([
				'html_title' => 'ernBnB - liste des mes annonces',
				'rooms' => $rooms
			]);
		}

		else {
			View::render404();
		}
	}

    public function add_room(): void
	{

		$view = new View( 'rooms\add_room' );

		$view->render([
			'html_title' => 'ernBnB - ajouter une annonce'
		]);
	}

	public function register_room( ServerRequest $request ): void
	{
		$post_data = $request->getParsedBody();
		$room = new Room();
		$room->address = $post_data['address'];
		$room->price = $post_data['price'];
		$room->size = $post_data['size'];
		$room->sleeping = $post_data['sleeping'];
		$room->id_user = $post_data['id_user'];
		$room->description = $post_data['description'];

		//entire_room = 0
		//private_room = 1
		//shared_room = 2

		switch( $post_data['type'] ) {
			case 'entire_room' :
				$room->type = 0;
			break;

			case 'private_room' :
				$room->type = 1;
			break;

			case 'shared_room' :
				$room->type = 2;
			break;
		}
		
		// Utilisation de create
		$created_room = null;

		try {
			$created_room = RepositoryManager::getRm()->getRoomRepository()->create( $room );
		}
		catch( PDOException $e ) {
			var_dump( $e );
			View::render500();
		}

		header( 'Location: /mes_annonces' );
	}

    public function show( int $id ): void
	{
		$room = RepositoryManager::getRm()->getRoomRepository()->findById( $id );

		// Si la chambre n'existe pas on lance la page 404
		if( is_null( $room ) ) {
			View::render404();
			return;
		}

		$view = new View( 'rooms\room_details' );
		$view->render([
			'html_title' => $room->address,
			'room' => $room
		]);
        
	}
}