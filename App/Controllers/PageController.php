<?php

namespace App\Controllers;

use App\Models\User;
use Laminas\Diactoros\ServerRequest;

use App\Repositories\RepositoryManager;
use App\View;
use PDOException;

class PageController
{
	/**
	 * Site Home page
	 */

	// On rajoute la fonction qui appelle la view de notre page
	public function index(): void
	{

		$view = new View( 'pages\home' );

		$view->render([
			'html_title' => 'ernBnB',
			'rooms' => RepositoryManager::getRm()->getRoomRepository()->findThree()
		]);
	}


	public function login(): void
	{

		$view = new View( 'pages\login' );

		$view->render([
			'html_title' => 'Connexion'
		]);
	}

	public function processLogin( ServerRequest $request ): void
	{
		$post_data = $request->getParsedBody();

		// TODO_normaly: contrôler la saisie (saisie vide, format d'email, etc.)
		$success = RepositoryManager::getRm()->getUserRepository()->auth( $post_data[ 'user_name' ], $post_data[ 'password' ] );

		if( ! $success ) {
			header( 'Location: /login' );
			die();
		}

		header( 'Location: /' );
	}

	public function add_user( ServerRequest $request ): void {
		$post_data = $request->getParsedBody();
		$user = new User();
		$user->user_name = $post_data['user_name'];
		$user->password = User::hashPassword( $post_data['password'] );

		if($post_data['role'] == 'is_owner'){
			
			$user->role = 0;

		}
		else{
			$user->role = 1;
		}

		
		// Utilisation de create
		$created_user = null;

		try {
			$created_user = RepositoryManager::getRm()->getUserRepository()->create( $user );
		}
		catch( PDOException $e ) {
			var_dump( $e );
			View::render500();
		}

		header( 'Location: /' );
	}

	public function logout(): void
	{
		// Demande au navigateur de périmer le cookie de session
		// ini_get() récupère une configuration de php.ini
		if( ini_get('session.use_cookies') ) {
			$params = session_get_cookie_params();

			setcookie(
				session_name(),
				'',
				time() - 42000,
				$params['path'],
				$params['domain'],
				$params['secure'],
				$params['httponly']
			);
		}

		// Efface la session sur le serveur
		session_destroy();

		// Demande au navigateur une redirection vers l'accueil
		header( 'Location: /' );
		die();
	}

	public function register(): void
	{

		$view = new View( 'pages\register' );

		$view->render([
			'html_title' => 'Inscription'
		]);
	}
}